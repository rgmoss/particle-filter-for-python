[build-system]
requires = ["hatchling ~= 1.18"]
build-backend = "hatchling.build"

[project]
name = "pypfilt"
version = "0.8.5"
description = "Particle filters for Python"
readme = "README.rst"
requires-python = ">= 3.8"
license = {file = "LICENSE"}
authors = [
  {name = "Rob Moss", email = "rgmoss@unimelb.edu.au"}
]
maintainers = [
  {name = "Rob Moss", email = "rgmoss@unimelb.edu.au"}
]
classifiers = [
  "Development Status :: 4 - Beta",
  "Intended Audience :: Science/Research",
  "License :: OSI Approved :: BSD License",
  "Operating System :: OS Independent",
  "Programming Language :: Python",
  "Programming Language :: Python :: 3",
  "Topic :: Scientific/Engineering :: Mathematics",
]
dependencies = [
  "h5py ~= 3.0",
  "lhs ~= 0.4",
  "numpy >= 1.17",
  "packaging >= 21.3",
  "scipy ~= 1.4",
  "tomli ~= 2.0",
  "tomli-w ~= 1.0",
]

[project.optional-dependencies]
plot = [
  "matplotlib ~= 3.4",
]
tests = [
  'pytest',
  'pytest-cov ~= 4.0',
]

[project.urls]
homepage = "https://bitbucket.org/robmoss/particle-filter-for-python/"
repository = "https://bitbucket.org/robmoss/particle-filter-for-python/"
documentation = "https://pypfilt.readthedocs.io/en/latest/"
changelog = "https://pypfilt.readthedocs.io/en/latest/changelog.html"

[tool.pytest.ini_options]
addopts = """\
    --doctest-modules \
    --doctest-glob='*.rst' \
    --capture=no \
    --cov-report term \
    --cov-report html\
    """

[tool.ruff]
line-length = 78
target-version = "py38"

[tool.ruff.lint]
# Enable pyflakes (F), pycodestyle (E, W), flake8-bugbear (B), pyupgrade (UP),
# flake8-debugger (T10), NumPy (NPY), and flake8-pytest-style (PT).
# Also verify that "noqa" directives (used to suppress lint errors) are valid,
# in that they are suppressing lint errors that are present.
select = ["F", "E", "W", "B", "UP", "T10", "NPY", "PT", "RUF100"]
# Continue to allow the use of `format()` instead of f-strings.
ignore = ["UP032"]

[tool.ruff.lint.per-file-ignores]
# Ignore "Ambiguous variable name" for SIR state variable `I`.
"sir.py" = ["E741"]

[tool.ruff.format]
quote-style = "single"
docstring-code-format = true

[tool.hatch.build.targets.sdist]
only-include = ["src", "tests"]
