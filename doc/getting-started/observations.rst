.. _lorenz63-obs:

The observation model
=====================

To define the relationship between the Lorenz-63 simulation model and observations of this system, we need to define an **observation model**.
When the observation model can be described using a standard probability distribution, we only need to create a :class:`~pypfilt.obs.Univariate` subclass that returns a `SciPy distribution <https://docs.scipy.org/doc/scipy/reference/stats.html>`__ for the given state vectors :math:`\mathbf{x_t}`.

For simplicity, we assume that :math:`x(t)`, :math:`y(t)`, and :math:`z(t)` can be directly observed (define the observed values as :math:`X_t`, :math:`Y_t`, and :math:`Z_t`, respectively) and that the observation error is distributed normally with zero mean and standard deviation :math:`\sigma = 1.5`:

.. math::

   X_t \sim \mathcal{N}(\mu = x(t), \sigma = 1.5) \\
   Y_t \sim \mathcal{N}(\mu = y(t), \sigma = 1.5) \\
   Z_t \sim \mathcal{N}(\mu = z(t), \sigma = 1.5)

The implementation of these observation models is straightforward, and comprises two steps.
First, extract the expected value for each particle from the state vectors, then construct a corresponding normal distribution for each particle:

.. literalinclude:: ../../src/pypfilt/examples/lorenz.py
   :pyobject: ObsLorenz63
   :lines: 1,17-
   :emphasize-lines: 4

.. note:: pypfilt_ support multiple observation models.
   Each observation model is associated with a unique identifier (an **"observation unit"**) that is used to identify the observations related to this model.
   Here, we use the observation unit (``self.unit``) to identify the field in the state vector that is being observed (see the highlighted line, above).
   This allows us to use three instances of the :class:`~pypfilt.examples.lorenz.ObsLorenz63` class to observe :math:`x(t)`, :math:`y(t)`, and :math:`z(t)`.

   We could define an observation model specifically for :math:`x(t)` (which is named ``'x'`` in the state vector) by replacing the highlighted line above with:

   .. code-block:: python

      expect = snapshot.state_vec['x']
