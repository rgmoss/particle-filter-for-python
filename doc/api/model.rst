pypfilt.model
=============

.. module:: pypfilt.model

All simulation models should derive from the following base class:

.. autoclass:: pypfilt.model.Model
   :members:

Ordinary differential equations
-------------------------------

Models that can be expressed as a system of ordinary differential equations should instead derive from the :class:`OdeModel` class, which provides a convenient wrapper around `scipy.integrate.solve_ivp() <https://docs.scipy.org/doc/scipy/reference/generated/scipy.integrate.solve_ivp.html>`__.

.. autoclass:: pypfilt.model.OdeModel
   :members: d_dt

Performing "mini-steps"
-----------------------

For simulation models that require very small time-steps, it may be desirable to avoid recording the particle states at each time-step.
This can be achieved by using a large time-step (e.g., setting ``steps_per_unit = 1``) and dividing each time-step into a number of "mini-steps" that will not be recorded.
See :ref:`performing-ministeps` for several examples of using the :func:`ministeps` decorator.

.. autofunction:: pypfilt.model.ministeps
