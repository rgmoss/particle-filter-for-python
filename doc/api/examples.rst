pypfilt.examples
================

.. py:module:: pypfilt.examples

The :mod:`pypfilt.examples` module includes a chaotic dynamical system, a predator-prey system, a Gaussian random walk, and several epidemic SIR models.

.. _examples-lorenz63:

Dynamical systems models
------------------------

.. py:module:: pypfilt.examples.lorenz

.. autoclass:: Lorenz63

.. autoclass:: ObsLorenz63

.. autofunction:: save_lorenz63_scenario_files

.. autofunction:: lorenz63_simulate_toml

.. autofunction:: lorenz63_forecast_toml

.. autofunction:: lorenz63_forecast_regularised_toml

.. autofunction:: lorenz63_all_scenarios_toml

Predator-prey system
--------------------

.. py:module:: pypfilt.examples.predation

Models
^^^^^^

.. autoclass:: LotkaVolterra
   :members:

.. autoclass:: ObsModel
   :members:

Example files
^^^^^^^^^^^^^

.. autofunction:: write_example_files

.. autofunction:: example_toml_data

.. autofunction:: example_obs_x_data

.. autofunction:: example_obs_y_data

.. autofunction:: example_toml_datetime_data

.. autofunction:: example_obs_x_datetime_data

.. autofunction:: example_obs_y_datetime_data

Generating forecasts
^^^^^^^^^^^^^^^^^^^^

.. autofunction:: forecast

.. autofunction:: plot

.. autofunction:: plot_params

.. autofunction:: plot_forecasts


Other functions
^^^^^^^^^^^^^^^^^^^^

.. autofunction:: default_priors

.. autofunction:: predation_instance

.. autofunction:: predation_scalar_instance

.. autofunction:: predation_datetime_instance

.. autofunction:: apply_ground_truth_prior

.. autofunction:: save_scalar_observations

Gaussian random walk
--------------------

.. py:module:: pypfilt.examples.simple

Models
^^^^^^

.. autoclass:: GaussianWalk

.. autoclass:: GaussianObs

Support functions
^^^^^^^^^^^^^^^^^

.. autofunction:: gaussian_walk_toml_data

.. autofunction:: gaussian_walk_instance

Epidemic SIR models
-------------------

.. py:module:: pypfilt.examples.sir

Models
^^^^^^

.. autoclass:: SirCtmc

.. autoclass:: SirDtmc

.. autoclass:: SirOdeEuler

.. autoclass:: SirOdeRk

.. autoclass:: SirSde

.. autoclass:: SirObs
   :members:

Example files
^^^^^^^^^^^^^

.. autofunction:: sir_toml_data

Epidemic SIRV models
--------------------

.. py:module:: pypfilt.examples.sirv

Models
^^^^^^

.. autoclass:: SirvOde

.. autoclass:: Incidence

.. autoclass:: FinalSize

Example files
^^^^^^^^^^^^^

.. autofunction:: sirv_toml_data
