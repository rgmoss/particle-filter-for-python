"""Test cases for the pypfilt.io module."""

import collections
import datetime
import h5py
import numpy as np
import os
import pytest

import pypfilt
from pypfilt.io import read_table, date_column, write_table
from pypfilt.io import read_fields, time_field, string_field


def test_read_datetime():
    # Test data: sequential dates with Fibonacci sequence.
    content = """
    time count
    2020-01-01 1
    2020-01-02 1
    2020-01-03 2
    2020-01-04 3
    2020-01-05 5
    2020-01-06 8
    2020-01-07 13
    2020-01-08 21
    2020-01-09 34
    """
    expect_rows = 9
    expect_count = [1, 1]
    for i in range(expect_rows - 2):
        expect_count.append(expect_count[i] + expect_count[i + 1])

    # Save this data to a temporary data file.
    path = 'test_read_datetime.ssv'
    with open(path, encoding='utf-8', mode='w') as f:
        f.write(content)

    # Read the data and then remove the data file.
    columns = [
        date_column('time'),
        ('count', np.int64),
    ]
    df = read_table(path, columns)
    os.remove(path)

    # Check that we received the expected number of rows.
    assert len(df) == expect_rows

    # Check that each row has the expected content.
    for ix, row in enumerate(df):
        assert isinstance(row['time'], datetime.datetime)
        assert row['time'].year == 2020
        assert row['time'].month == 1
        assert row['time'].day == ix + 1
        assert row['count'] == expect_count[ix]


def test_save_and_load_datetime_dataset():
    """
    Ensure that we recover the expected data when using
    ``pypfilt.io.save_dataset()`` and ``pypfilt.io.load_dataset()`` with a
    ``Datetime`` time scale.
    """
    # Create a minimal context that only holds the time_scale component.
    Context = collections.namedtuple('Context', ['component'])
    time_scale = pypfilt.Datetime()
    ctx = Context(component={'time': time_scale})

    hdf5_file = 'test_save_and_load_datetime_dataset.hdf5'
    hdf5_group = 'test_group'
    hdf5_dataset = 'test_data'

    fields = [
        pypfilt.io.time_field('forecast_time'),
        pypfilt.io.time_field('time'),
        ('value', np.float64),
    ]
    dtype = pypfilt.io.fields_dtype(ctx, fields)
    values = [
        (
            datetime.datetime(2021, 11, 21),
            datetime.datetime(2021, 11, 22),
            3.0,
        ),
        (
            datetime.datetime(2021, 11, 21),
            datetime.datetime(2021, 11, 23),
            9.0,
        ),
        (
            datetime.datetime(2021, 11, 21),
            datetime.datetime(2021, 11, 24),
            27.0,
        ),
        (
            datetime.datetime(2021, 11, 21),
            datetime.datetime(2021, 11, 25),
            81.0,
        ),
    ]
    table = np.array(values, dtype=dtype)

    expected_time_cols = ['forecast_time', 'time']

    # Ensure this table has metadata that identifies the time column.
    assert table.dtype.metadata is not None
    assert table.dtype.metadata['time_columns'] == expected_time_cols

    with h5py.File(hdf5_file, 'w') as f:
        group = f.create_group(hdf5_group)
        ds = pypfilt.io.save_dataset(time_scale, group, hdf5_dataset, table)
        # Ensure the dataset has an attribute that identifies the time column.
        assert 'time_columns' in ds.attrs
        assert len(ds.attrs['time_columns']) == len(expected_time_cols)
        for col in expected_time_cols:
            assert col.encode() in ds.attrs['time_columns']

    with h5py.File(hdf5_file, 'r') as f:
        group = f[hdf5_group]
        dataset = group[hdf5_dataset]
        new_table = pypfilt.io.load_dataset(time_scale, dataset)

    # Ensure we received an identical NumPy array.
    assert new_table.dtype.metadata is not None
    assert new_table.dtype.metadata['time_columns'] == expected_time_cols
    assert np.array_equal(table, new_table)

    # Ensure that load_summary_table returns the same data.
    dataset_path = f'{hdf5_group}/{hdf5_dataset}'
    table_2 = pypfilt.io.load_summary_table(
        time_scale, hdf5_file, dataset_path, subset=()
    )
    assert table_2.dtype.metadata is not None
    assert table_2.dtype.metadata['time_columns'] == expected_time_cols
    assert np.array_equal(table, table_2)

    # Ensure that loading slices return the expected subset.
    subset = slice(None, None, 3)
    table_3 = pypfilt.io.load_summary_table(
        time_scale, hdf5_file, dataset_path, subset=subset
    )
    assert np.array_equal(table[subset], table_3)

    # Ensure we can provide a Context-like object instead of a time scale.
    table_ctx = pypfilt.io.load_summary_table(
        ctx, hdf5_file, dataset_path, subset=()
    )
    assert table_ctx.dtype.metadata is not None
    assert table_ctx.dtype.metadata['time_columns'] == expected_time_cols
    assert np.array_equal(table, table_ctx)

    # Ensure we can provide an Instance-like object instead of a time scale.
    Instance = collections.namedtuple('Instance', ['time_scale'])
    instance = Instance(time_scale=lambda: time_scale)
    table_inst = pypfilt.io.load_summary_table(
        instance, hdf5_file, dataset_path, subset=()
    )
    assert table_inst.dtype.metadata is not None
    assert table_inst.dtype.metadata['time_columns'] == expected_time_cols
    assert np.array_equal(table, table_inst)

    # Clean up.
    os.remove(hdf5_file)


def test_save_and_load_scalar_dataset():
    """
    Ensure that we recover the expected data when using
    ``pypfilt.io.save_dataset()`` and ``pypfilt.io.load_dataset()`` with a
    ``Scalar`` time scale.
    """
    # Create a minimal context that only holds the time_scale component.
    Context = collections.namedtuple('Context', ['component'])
    time_scale = pypfilt.Scalar()
    ctx = Context(component={'time': time_scale})

    hdf5_file = 'test_save_and_load_scalar_dataset.hdf5'
    hdf5_group = 'test_group'
    hdf5_dataset = 'test_data'

    fields = [
        pypfilt.io.time_field('forecast_time'),
        pypfilt.io.time_field('time'),
        ('value', np.float64),
    ]
    dtype = pypfilt.io.fields_dtype(ctx, fields)
    values = [
        (1.0, 1.0, 3.0),
        (1.0, 1.5, 9.0),
        (1.0, 2.0, 27.0),
        (1.0, 2.5, 81.0),
    ]
    table = np.array(values, dtype=dtype)

    expected_time_cols = ['forecast_time', 'time']

    # Ensure this table has metadata that identifies the time column.
    assert table.dtype.metadata is not None
    assert table.dtype.metadata['time_columns'] == expected_time_cols

    with h5py.File(hdf5_file, 'w') as f:
        group = f.create_group(hdf5_group)
        ds = pypfilt.io.save_dataset(time_scale, group, hdf5_dataset, table)
        # Ensure the dataset has an attribute that identifies the time column.
        assert 'time_columns' in ds.attrs
        assert len(ds.attrs['time_columns']) == len(expected_time_cols)
        for col in expected_time_cols:
            assert col.encode() in ds.attrs['time_columns']

    with h5py.File(hdf5_file, 'r') as f:
        group = f[hdf5_group]
        dataset = group[hdf5_dataset]
        new_table = pypfilt.io.load_dataset(time_scale, dataset)

    # Ensure we received an identical NumPy array.
    assert new_table.dtype.metadata is not None
    assert new_table.dtype.metadata['time_columns'] == expected_time_cols
    assert np.array_equal(table, new_table)

    # Ensure that load_summary_table returns the same data.
    dataset_path = f'{hdf5_group}/{hdf5_dataset}'
    table_2 = pypfilt.io.load_summary_table(
        time_scale, hdf5_file, dataset_path, subset=()
    )
    assert table_2.dtype.metadata is not None
    assert table_2.dtype.metadata['time_columns'] == expected_time_cols
    assert np.array_equal(table, table_2)

    # Ensure that loading slices return the expected subset.
    subset = slice(None, None, 3)
    table_3 = pypfilt.io.load_summary_table(
        time_scale, hdf5_file, dataset_path, subset=subset
    )
    assert np.array_equal(table[subset], table_3)

    # Ensure we can provide a Context-like object instead of a time scale.
    table_ctx = pypfilt.io.load_summary_table(
        ctx, hdf5_file, dataset_path, subset=()
    )
    assert table_ctx.dtype.metadata is not None
    assert table_ctx.dtype.metadata['time_columns'] == expected_time_cols
    assert np.array_equal(table, table_ctx)

    # Ensure we can provide an Instance-like object instead of a time scale.
    Instance = collections.namedtuple('Instance', ['time_scale'])
    instance = Instance(time_scale=lambda: time_scale)
    table_inst = pypfilt.io.load_summary_table(
        instance, hdf5_file, dataset_path, subset=()
    )
    assert table_inst.dtype.metadata is not None
    assert table_inst.dtype.metadata['time_columns'] == expected_time_cols
    assert np.array_equal(table, table_inst)

    # Clean up.
    os.remove(hdf5_file)


def test_read_datetime_fields():
    """
    Test reading datetime fields with read_fields().
    """
    content = """
    time count
    2020-01-01 1
    2020-01-02 1
    2020-01-03 2
    2020-01-04 3
    2020-01-05 5
    2020-01-06 8
    2020-01-07 13
    2020-01-08 21
    2020-01-09 34
    """
    expect_rows = 9
    expect_count = [1, 1]
    for i in range(expect_rows - 2):
        expect_count.append(expect_count[i] + expect_count[i + 1])

    # Save this data to a temporary data file.
    path = 'test_read_datetime_fields.ssv'
    with open(path, encoding='utf-8', mode='w') as f:
        f.write(content)

    # Read the data and then remove the data file.
    time_scale = pypfilt.Datetime()
    fields = [time_field('time'), ('count', np.int64)]
    df = read_fields(time_scale, path, fields)
    os.remove(path)

    # Check that the time column is identified as containing time values.
    assert df.dtype.metadata is not None
    assert 'time_columns' in df.dtype.metadata
    assert df.dtype.metadata['time_columns'] == ['time']

    # Check that we received the expected number of rows.
    assert len(df) == expect_rows

    # Check that each row has the expected content.
    for ix, row in enumerate(df):
        assert isinstance(row['time'], datetime.datetime)
        assert row['time'].year == 2020
        assert row['time'].month == 1
        assert row['time'].day == ix + 1
        assert row['count'] == expect_count[ix]


def test_read_string():
    """
    Test reading string fields with read_table().
    """

    content = """
    time count
    2020-01-01 1
    2020-01-02 1
    2020-01-03 2
    2020-01-04 3
    2020-01-05 5
    2020-01-06 8
    2020-01-07 13
    2020-01-08 21
    2020-01-09 34
    """

    # Save this data to a temporary data file.
    path = 'test_read_string_fields.ssv'
    with open(path, encoding='utf-8', mode='w') as f:
        f.write(content)

    # Read the data and then remove the data file.
    columns = [string_field('time'), ('count', np.int64)]
    error_regex = '.*String column time is not supported.*'
    with pytest.raises(ValueError, match=error_regex):
        read_table(path, columns)

    os.remove(path)


def test_read_string_fields():
    """
    Test reading string fields with read_fields().
    """

    content = """
    time count
    2020-01-01 1
    2020-01-02 1
    2020-01-03 2
    2020-01-04 3
    2020-01-05 5
    2020-01-06 8
    2020-01-07 13
    2020-01-08 21
    2020-01-09 34
    """

    # Save this data to a temporary data file.
    path = 'test_read_string_fields.ssv'
    with open(path, encoding='utf-8', mode='w') as f:
        f.write(content)

    # Read the data and then remove the data file.
    time_scale = pypfilt.Datetime()
    fields = [string_field('time'), ('count', np.int64)]
    error_regex = '.*String column time is not supported.*'
    with pytest.raises(ValueError, match=error_regex):
        read_fields(time_scale, path, fields)

    os.remove(path)


def test_write_table_datetime():
    # NOTE: must use a datetime format that does not include spaces.
    settings = {'time': {'datetime_formats': ['%Y-%m-%d']}}
    time_scale = pypfilt.time.Datetime(settings=settings)
    fields = [time_field('time'), ('count', np.int64), ('extra', np.float64)]
    save_columns = ['time', 'count']
    table = np.array(
        [
            (datetime.datetime(2022, 8, 1), 2, 10.0),
            (datetime.datetime(2022, 8, 2), 3, 20.0),
            (datetime.datetime(2022, 8, 3), 5, 30.0),
        ],
        dtype=pypfilt.io.fields_dtype(time_scale, fields),
    )
    path = 'test_write_table_datetime.ssv'
    write_table(path, table, time_scale, columns=save_columns)
    table_in = read_fields(time_scale, path, fields[:2])
    assert np.array_equal(table[save_columns], table_in)
    os.remove(path)


def test_write_table_scalar():
    time_scale = pypfilt.time.Scalar()
    fields = [time_field('time'), ('count', np.int64), ('extra', np.float64)]
    save_columns = ['time', 'count']
    table = np.array(
        [
            (1.0, 2, 10.0),
            (2.0, 3, 20.0),
            (3.0, 5, 30.0),
        ],
        dtype=pypfilt.io.fields_dtype(time_scale, fields),
    )
    path = 'test_write_table_datetime.ssv'
    write_table(path, table, time_scale, columns=save_columns)
    table_in = read_fields(time_scale, path, fields[:2])
    assert np.array_equal(table[save_columns], table_in)
    os.remove(path)
