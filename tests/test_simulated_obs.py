"""
Ensure that we can control the number of particles and observations captured
by the SimulatedObs table.
"""

import numpy as np
import pypfilt
import pypfilt.examples.simple


def test_simulated_obs_settings():
    """
    Test the SimulatedObs table directly.
    """
    time_units = 5
    start = 0.0
    until = start + time_units
    ensemble_size = 10

    for particle_count in [1, 5, 10, 20, None]:
        for obs_per_particle in [1, 2, 4, None]:
            inst = pypfilt.examples.simple.gaussian_walk_instance()
            inst.settings['filter']['particles'] = ensemble_size

            inst.settings['time']['start'] = start
            inst.settings['time']['until'] = until

            sim_obs = inst.settings['summary']['tables']['sim_obs']
            sim_obs['particle_count'] = particle_count
            sim_obs['observations_per_particle'] = obs_per_particle
            if particle_count is None:
                del sim_obs['particle_count']
            if obs_per_particle is None:
                del sim_obs['observations_per_particle']

            # Simulate the observations using pypfilt.fit().
            # NOTE: since there are no observations, the summary tables will
            # consider this to be a forecasting pass.
            ctx = inst.build_context(obs_tables={})
            results = pypfilt.fit(ctx, filename=None)
            table = results.estimation.tables['sim_obs']

            # Simulate the observations using pypfilt.forecast().
            # Only do this for a single iteration of these nested loops.
            if particle_count is None and obs_per_particle is None:
                ctx_fx = inst.build_context(obs_tables={})
                results_fx = pypfilt.forecast(ctx_fx, [start], filename=None)
                table_fx = results_fx.forecasts[0.0].tables['sim_obs']
                # Ensure we obtain identical tables (see note above).
                assert np.array_equal(table, table_fx)

            n_px = particle_count or ensemble_size
            n_obs = obs_per_particle or 1
            expected_rows = n_px * n_obs * (time_units + 1)
            assert table.shape == (expected_rows,)

            assert len(np.unique(table['time'])) == (time_units + 1)
            assert sum(table['time'] == table['time'][0]) == n_px * n_obs

            # Check that the initial values for the first two particles do not
            # overlap.
            # This is an indirect (and imperfect) way to check that we are
            # simulating multiple observations from each particle.
            if n_px == ensemble_size and n_obs >= 2:
                mask_init = table['time'] == table['time'][0]
                values = table['value'][mask_init]
                values_px0 = values[:n_obs]
                values_px1 = values[n_obs : 2 * n_obs]
                px1_in_px0 = (min(values_px0) <= values_px1) & (
                    values_px1 <= max(values_px0)
                )
                px0_in_px1 = (min(values_px0) <= values_px1) & (
                    values_px1 <= max(values_px0)
                )
                assert not any(px1_in_px0)
                assert not any(px0_in_px1)

            # Check that the initial values for the first two particles do not
            # overlap.
            # This is an indirect (and imperfect) way to check that we are
            # simulating multiple observations from each particle.
            if n_px == 2 * ensemble_size and n_obs >= 2:
                mask_init = table['time'] == table['time'][0]
                values = table['value'][mask_init]
                values_px0 = values[: 2 * n_obs]
                values_px1 = values[2 * n_obs : 4 * n_obs]
                px1_in_px0 = (min(values_px0) <= values_px1) & (
                    values_px1 <= max(values_px0)
                )
                px0_in_px1 = (min(values_px0) <= values_px1) & (
                    values_px1 <= max(values_px0)
                )
                assert not any(px1_in_px0)
                assert not any(px0_in_px1)


def test_simulate_from_model():
    """
    Test that pypfilt.simulate_from_model() behaves as expected.
    """
    time_units = 5
    start = 0.0
    until = start + time_units
    ensemble_size = 10
    particle_count = 5
    obs_per_particle = 4

    inst = pypfilt.examples.simple.gaussian_walk_instance()
    inst.settings['filter']['particles'] = ensemble_size

    inst.settings['time']['start'] = start
    inst.settings['time']['until'] = until

    sim_obs = inst.settings['summary']['tables']['sim_obs']
    sim_obs['particle_count'] = particle_count
    sim_obs['observations_per_particle'] = obs_per_particle

    tables = pypfilt.simulate_from_model(
        inst, particles=particle_count, observations=obs_per_particle
    )

    assert len(tables) == 1
    assert 'x' in tables
    expected_rows = (time_units + 1) * particle_count * obs_per_particle
    assert len(tables['x']) == expected_rows
