import h5py
import numpy as np
from pathlib import Path
import pypfilt
import pypfilt.examples.predation
from pypfilt.scenario import override_dict


def test_adaptive_fit_predation_eff_frac():
    """
    Test the behaviour of adaptive_fit() against the predator-prey example.
    """
    pypfilt.examples.predation.write_example_files()
    config_file = 'predation.toml'
    instances = list(pypfilt.load_instances(config_file))
    instance = instances[0]

    # Ensure that pypfilt returns the particle history matrices.
    override_dict(
        instance.settings, {'filter': {'results': {'save_history': True}}}
    )

    context = instance.build_context()

    method = 'fit_effective_fraction'
    context.settings['filter']['adaptive_fit']['method'] = method

    # Record the default range of credible intervals.
    tables = context.settings['summary']['tables']
    del tables['model_cints']['credible_intervals']
    del tables['forecasts']['credible_intervals']

    # Run the adaptive fitting and save the results of the final pass.
    output_file = Path('test_adaptive_fit_predation_eff_frac.hdf5')
    results = pypfilt.adaptive_fit(context, output_file)
    fits = results.adaptive_fits
    exponents = list(fits.keys())
    assert all(isinstance(exponent, float) for exponent in exponents)

    # Ensure that the exponents increase monotonically from some initial
    # strictly positive value and end at unity.
    assert len(exponents) > 2
    assert exponents[0] > 0
    assert exponents[-1] == 1
    assert all(np.diff(exponents) > 0)

    # Check that the parameters do not evolve during each estimation pass,
    # except when the particles are reweighted and resampled at the very end.
    params = ['alpha', 'beta', 'gamma', 'delta']
    for result in fits.values():
        hist_mat = result.history.matrix

        # NOTE: the parameters should remain fixed over each pass.
        # However, their final values will differ because we reweight and
        # resample at the end of each pass.
        for param in params:
            values = hist_mat['state_vec'][param]
            for step in range(1, hist_mat.shape[0] - 1):
                assert np.array_equal(values[0], values[step])
            # Ensure that resampling changes the parameter values.
            assert not np.array_equal(values[0], values[-1])

    # Ensure that parameters remain fixed over the entire final pass, or until
    # the first resampling event (if any).
    result = results.estimation
    hist_mat = result.history.matrix
    model_cints = result.tables['model_cints']
    forecasts = result.tables['forecasts']
    for param in params:
        resampled = False
        values = hist_mat['state_vec'][param]
        for step in range(1, hist_mat.shape[0]):
            if any(hist_mat['resampled'][step]):
                resampled = True
            if resampled:
                assert not np.array_equal(values[0], values[step])
            else:
                assert np.array_equal(values[0], values[step])

    # Check that only the tables from the final estimation pass were saved.
    time_scale = pypfilt.Scalar()
    with h5py.File(output_file) as f:
        tbl_fx = pypfilt.io.load_dataset(time_scale, f['/tables/forecasts'])
        tbl_model = pypfilt.io.load_dataset(
            time_scale, f['/tables/model_cints']
        )
        fits_fx = pypfilt.io.load_dataset(
            time_scale, f['/adaptive_fit/forecasts']
        )
        fits_model = pypfilt.io.load_dataset(
            time_scale, f['/adaptive_fit/model_cints']
        )

    assert np.array_equal(tbl_fx, forecasts)
    assert np.array_equal(tbl_model, model_cints)

    # Check that the aggregated adaptive fit tables contain the results from
    # each pass.
    exps = np.unique(fits_model['adaptive_fit_exponent'])
    assert len(exps) == len(fits)
    assert np.array_equal(exps, np.unique(fits_fx['adaptive_fit_exponent']))
    for ix, exp in enumerate(exps):
        assert np.allclose(exp, exponents[ix])
        expect_fx = fits[exp].tables['forecasts']
        mask = fits_fx['adaptive_fit_exponent'] == exp
        cols = list(expect_fx.dtype.names)
        assert np.array_equal(expect_fx, fits_fx[mask][cols])
        expect_model = fits[exp].tables['model_cints']
        mask = fits_model['adaptive_fit_exponent'] == exp
        cols = list(expect_model.dtype.names)
        assert np.array_equal(expect_model, fits_model[mask][cols])

    # Remove generated files.
    pypfilt.examples.predation.remove_example_files()
    output_file.unlink()


def test_adaptive_fit_predation_fixed_exps():
    """
    Test the behaviour of adaptive_fit() against the predator-prey example.
    """
    pypfilt.examples.predation.write_example_files()
    config_file = 'predation.toml'
    instances = list(pypfilt.load_instances(config_file))
    instance = instances[0]

    # Ensure that pypfilt returns the particle history matrices.
    override_dict(
        instance.settings, {'filter': {'results': {'save_history': True}}}
    )

    context = instance.build_context()

    method = 'fixed_exponents'
    context.settings['filter']['adaptive_fit']['method'] = method
    context.settings['filter']['adaptive_fit']['exponent_step'] = 0.25

    # Record the default range of credible intervals.
    tables = context.settings['summary']['tables']
    del tables['model_cints']['credible_intervals']
    del tables['forecasts']['credible_intervals']

    # Run the adaptive fitting and save the results of the final pass.
    output_file = Path('test_adaptive_fit_predation_fixed_exps.hdf5')
    results = pypfilt.adaptive_fit(context, output_file)
    fits = results.adaptive_fits
    exponents = list(fits.keys())
    assert all(isinstance(exponent, float) for exponent in exponents)

    # Ensure that the exponents increase monotonically as expected.
    assert len(exponents) == 4
    assert np.allclose(exponents, [0.25, 0.50, 0.75, 1.0])

    # Check that the parameters evolve during each estimation pass.
    params = ['alpha', 'beta', 'gamma', 'delta']
    for result in fits.values():
        hist_mat = result.history.matrix
        for param in params:
            resampled = False
            values = hist_mat['state_vec'][param]
            for step in range(1, hist_mat.shape[0]):
                if any(hist_mat['resampled'][step]):
                    resampled = True
                if resampled:
                    assert not np.array_equal(values[0], values[step])
                elif not np.array_equal(values[0], values[step]):
                    assert np.array_equal(values[0], values[step])

    # Ensure that parameters vary over the final pass.
    result = results.estimation
    hist_mat = result.history.matrix
    model_cints = result.tables['model_cints']
    forecasts = result.tables['forecasts']
    for param in params:
        resampled = False
        values = hist_mat['state_vec'][param]
        for step in range(1, hist_mat.shape[0]):
            if any(hist_mat['resampled'][step]):
                resampled = True
            if resampled:
                assert not np.array_equal(values[0], values[step])
            else:
                assert np.array_equal(values[0], values[step])

    # Check that only the tables from the final estimation pass were saved.
    time_scale = pypfilt.Scalar()
    with h5py.File(output_file) as f:
        tbl_fx = pypfilt.io.load_dataset(time_scale, f['/tables/forecasts'])
        tbl_model = pypfilt.io.load_dataset(
            time_scale, f['/tables/model_cints']
        )
        fits_fx = pypfilt.io.load_dataset(
            time_scale, f['/adaptive_fit/forecasts']
        )
        fits_model = pypfilt.io.load_dataset(
            time_scale, f['/adaptive_fit/model_cints']
        )

    assert np.array_equal(tbl_fx, forecasts)
    assert np.array_equal(tbl_model, model_cints)

    # Check that the aggregated adaptive fit tables contain the results from
    # each pass.
    exps = np.unique(fits_model['adaptive_fit_exponent'])
    assert len(exps) == len(fits)
    assert np.array_equal(exps, np.unique(fits_fx['adaptive_fit_exponent']))
    for ix, exp in enumerate(exps):
        assert np.allclose(exp, exponents[ix])
        expect_fx = fits[exp].tables['forecasts']
        mask = fits_fx['adaptive_fit_exponent'] == exp
        cols = list(expect_fx.dtype.names)
        assert np.array_equal(expect_fx, fits_fx[mask][cols])
        expect_model = fits[exp].tables['model_cints']
        mask = fits_model['adaptive_fit_exponent'] == exp
        cols = list(expect_model.dtype.names)
        assert np.array_equal(expect_model, fits_model[mask][cols])

    # Remove generated files.
    pypfilt.examples.predation.remove_example_files()
    output_file.unlink()


def test_adaptive_fit_forecast():
    """
    Test using adaptive_fit() for the estimation pass when forecasting.
    """
    pypfilt.examples.predation.write_example_files()
    config_file = 'predation.toml'
    instances = list(pypfilt.load_instances(config_file))
    instance = instances[0]
    context = instance.build_context()

    method = 'fixed_exponents'
    context.settings['filter']['adaptive_fit']['method'] = method
    context.settings['filter']['adaptive_fit']['exponent_step'] = 0.25

    # Record the default range of credible intervals.
    tables = context.settings['summary']['tables']
    del tables['model_cints']['credible_intervals']
    del tables['forecasts']['credible_intervals']

    # Run the forecasts, using adaptive fitting for the estimation pass.
    context.settings['filter']['adaptive_fit']['enabled'] = True
    output_file = Path('test_adaptive_fit_forecast.hdf5')
    fs_times = [5.0, 10.0]
    results = pypfilt.forecast(context, fs_times, output_file)

    # Check that each forecast was recorded.
    for fs_time in fs_times:
        assert fs_time in results.forecasts
    # Check that the final estimation pass was recorded.
    assert results.estimation is not None
    # Check that the expected adaptive-fit passes were recorded.
    assert len(results.adaptive_fits) == 4
    exponents = list(results.adaptive_fits.keys())
    assert all(isinstance(exponent, float) for exponent in exponents)
    assert np.allclose(exponents, np.array([0.25, 0.5, 0.75, 1.0]))

    # Remove generated files.
    pypfilt.examples.predation.remove_example_files()
    output_file.unlink()
