"""Test cases for the SIRV model in ``pypfilt.examples.sirv``."""

import io
import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
from pathlib import Path
import scipy.stats

import pypfilt
import pypfilt.examples.sirv
import pypfilt.stats


def test_sirv_scenarios():
    # Run the baseline scenario and each intervention scenario.
    contexts, results = run_sirv_scenarios()

    # Check that parameter values sampled from the prior distributions meet
    # our expectations about being identical or different between scenarios.
    verify_prior_samples(contexts)

    output_dir = Path('doc').resolve() / 'how-to'

    # Plot daily incidence curves for each scenario.
    plot_epi_curves(results, output_dir / 'scenario-sirv-epi-curves.png')

    # Plot the distribution of epidemic final sizes for each scenario.
    plot_final_sizes(results, output_dir / 'scenario-sirv-final-sizes.png')

    # Plot the distribution of prevented infections for each scenario.
    plot_prevented_infections(
        results, output_dir / 'scenario-sirv-prevented-infections.png'
    )

    # Plot the correlation between prevented infections and model parameters.
    plot_prevented_infection_correlations(
        contexts, results, output_dir / 'scenario-sirv-correlations.png'
    )
    plot_prevented_infection_correlations_bars(
        contexts, results, output_dir / 'scenario-sirv-correlations-bars.png'
    )


def run_sirv_scenarios():
    """
    Run the baseline SIRV scenario and each intervention scenario.

    This returns a tuple that contains (a) a dictionary of simulation contexts
    for each scenario; and (b) a dictionary of the results for each scenario
    (:class:`pypfilt.pfilter.Result`).
    """
    source = io.StringIO(pypfilt.examples.sirv.sirv_toml_data())

    # Construct simulation contexts for each SIRV scenario.
    contexts = {
        instance.scenario_id: instance.build_context()
        for instance in pypfilt.load_instances(source)
    }
    # Collect the simulation results for each scenario.
    results = {
        name: pypfilt.fit(ctx, filename=None).estimation
        for name, ctx in contexts.items()
    }
    return contexts, results


def verify_prior_samples(contexts):
    """
    Verify that the parameter values sampled from the prior distributions for
    each scenario are either identical (e.g., pathogen parameters) or differ
    (e.g., intervention parameters).
    """
    baseline_prior = contexts['Baseline'].data['prior']
    reduce_R0_prior = contexts['Reduce R0'].data['prior']
    vaccination_prior = contexts['Vaccination'].data['prior']

    # Verify that samples for baseline parameters are identical.
    baseline_parameters = ['R0', 'gamma']
    for parameter in baseline_parameters:
        baseline_vals = baseline_prior[parameter]
        reduce_R0_vals = reduce_R0_prior[parameter]
        vaccination_vals = vaccination_prior[parameter]
        assert np.array_equal(baseline_vals, reduce_R0_vals)
        assert np.array_equal(baseline_vals, vaccination_vals)

    # Verify that vaccine parameter samples are different only for the
    # vaccination scenario.
    vacc_parameters = [
        param for param in baseline_prior if param.startswith('Vaccine_')
    ]
    for parameter in vacc_parameters:
        baseline_vals = baseline_prior[parameter]
        reduce_R0_vals = reduce_R0_prior[parameter]
        vaccination_vals = vaccination_prior[parameter]
        assert not np.array_equal(baseline_vals, vaccination_vals)
        assert np.array_equal(baseline_vals, reduce_R0_vals)

    # Verify that R0 parameter samples are different only for the R0 reduction
    # scenario.
    R0_parameters = [
        param for param in baseline_prior if param.startswith('R0_')
    ]
    for parameter in R0_parameters:
        baseline_vals = baseline_prior[parameter]
        reduce_R0_vals = reduce_R0_prior[parameter]
        vaccination_vals = vaccination_prior[parameter]
        assert not np.array_equal(baseline_vals, reduce_R0_vals)
        assert np.array_equal(baseline_vals, vaccination_vals)


def get_scenario_label(scenario_id):
    """
    Use maths formatting for "R0" in plot axis labels.
    """
    return scenario_id.replace('R0', '$R_0$')


def plot_epi_curves(results, png_file):
    """
    Plot the ensemble of daily incidence curves for each scenario.
    """
    fig, axs = plt.subplots(
        layout='constrained', nrows=len(results), sharex=True, sharey=True
    )

    # Plot the epidemic curves for each scenario in separate subplots.
    for ix, (scenario_id, result) in enumerate(results.items()):
        ax = axs[ix]

        daily_inc = result.tables['daily_inc']
        time = np.unique(daily_inc['time'])
        inc_curves = daily_inc['value'].reshape((len(time), -1))

        ax.plot(time, inc_curves, alpha=0.02, color='black')
        scenario_label = get_scenario_label(scenario_id)
        ax.set_title(f'Scenario: {scenario_label}')

        # Add the x-axis label to the bottom subplot.
        if ix == len(results) - 1:
            ax.set_xlabel('Time (days)')

        # Add the y-axis label to the middle subplot.
        if ix == 1:
            ax.set_ylabel('Daily incidence')

    fig.savefig(png_file, dpi=300, **png_kwargs())


def plot_final_sizes(results, png_file):
    """
    Plot the median and 95% credible interval for final size in each scenario.
    """
    ys, y_mins, y_maxs = get_final_sizes(results)
    fig, ax = plt.subplots(layout='constrained')

    # Plot horizontal lines for the baseline median and credible interval.
    ax.axhline(ys[0], linestyle='--', color='grey', linewidth=1)
    ax.axhline(y_mins[0], linestyle='--', color='grey', linewidth=1)
    ax.axhline(y_maxs[0], linestyle='--', color='grey', linewidth=1)

    # Plot the median and credible interval for each scenario.
    y_errs = np.array([ys - y_mins, y_maxs - ys])
    labels = [get_scenario_label(key) for key in results.keys()]
    ax.errorbar(labels, ys, yerr=y_errs, fmt='o')

    ax.set_xlabel('Scenario')
    ax.set_ylabel('Total infections')
    ax.set_ylim(bottom=0)

    fig.savefig(png_file, dpi=300, **png_kwargs())


def get_final_sizes(results):
    """
    Return the median and 95% credible interval for final size in each
    scenario.
    """
    ys = []
    y_mins = []
    y_maxs = []
    probs = [0.5, 0.025, 0.975]

    for result in results.values():
        final_sizes = result.tables['final_sizes']['final_size']
        weights = result.tables['final_sizes']['weight']
        quantiles = pypfilt.stats.qtl_wt(final_sizes, weights, probs)
        ys.append(quantiles[0])
        y_mins.append(quantiles[1])
        y_maxs.append(quantiles[2])

    return (np.array(ys), np.array(y_mins), np.array(y_maxs))


def get_prevented_infections(results, relative_to):
    """
    Calculate the number of infections that were prevented in each scenario,
    relative to the specified baseline scenario (``relative_to``).

    This returns a dictionary that maps intervention scenario names to arrays
    of prevented infection counts for each particle.
    """
    baseline_sizes = results[relative_to].tables['final_sizes']['final_size']
    prevented_infections = {}

    for scenario, result in results.items():
        # Ignore the baseline scenario.
        if scenario == relative_to:
            continue

        # Calculate the number of prevented infections for each particle,
        # relative to the corresponding particle in the baseline scenario.
        final_sizes = result.tables['final_sizes']['final_size']
        num_prevented = baseline_sizes - final_sizes

        prevented_infections[scenario] = num_prevented

    return prevented_infections


def plot_prevented_infections(results, png_file):
    """
    Plot the median and 95% credible interval for prevented infections in each
    intervention scenario.
    """
    prevented_infs = get_prevented_infections(results, relative_to='Baseline')

    ys = []
    y_mins = []
    y_maxs = []
    probs = [0.5, 0.025, 0.975]

    for num_prevented in prevented_infs.values():
        # NOTE: we know that each particle has the same weight, because we are
        # not conditioning on any observations.
        weights = np.ones(num_prevented.shape)
        quantiles = pypfilt.stats.qtl_wt(num_prevented, weights, probs)
        ys.append(quantiles[0])
        y_mins.append(quantiles[1])
        y_maxs.append(quantiles[2])

    ys = np.array(ys)
    y_mins = np.array(y_mins)
    y_maxs = np.array(y_maxs)

    fig, ax = plt.subplots(layout='constrained')

    # Plot the median and credible interval for each scenario.
    y_errs = np.array([ys - y_mins, y_maxs - ys])
    labels = [get_scenario_label(key) for key in prevented_infs.keys()]
    ax.errorbar(labels, ys, yerr=y_errs, fmt='o')

    ax.set_xlabel('Scenario')
    # Expand the x-axis limits to avoid plotting the two sets of results
    # at the extreme left and extreme right of the plot.
    padding = 0.5
    ax.set_xlim(left=0 - padding, right=len(prevented_infs) - 1 + padding)

    ax.set_ylabel('Prevented infections')
    ax.set_ylim(bottom=0)

    fig.savefig(png_file, dpi=300, **png_kwargs())


def plot_prevented_infection_correlations(contexts, results, png_file):
    """
    Plot Spearman correlation coefficients between the numbers of prevented
    infections in each scenario, and model parameters that characterise the
    pathogen (i.e., parameters that are not related to the interventions).
    """
    prevented_infs = get_prevented_infections(results, relative_to='Baseline')
    samples = {
        'R0': contexts['Baseline'].data['prior']['R0'],
        'gamma': contexts['Baseline'].data['prior']['gamma'],
    }
    samples['beta'] = samples['R0'] * samples['gamma']

    corrs = np.zeros((len(samples), len(prevented_infs)))
    for param_ix, values in enumerate(samples.values()):
        for scenario_ix, num_prevented in enumerate(prevented_infs.values()):
            coeff = scipy.stats.spearmanr(num_prevented, values).statistic
            corrs[param_ix, scenario_ix] = coeff

    fig, ax = plt.subplots(layout='constrained', figsize=[4.8, 4.8])
    ax.imshow(corrs, vmin=-1, vmax=1)

    for x in range(len(prevented_infs)):
        for y in range(len(samples)):
            ax.text(
                x,
                y,
                f'{corrs[y, x]: 0.3f}',
                ha='center',
                va='center',
                color='white',
                fontsize='x-large',
            )

    ax.set_title('Correlation with infections prevented')
    ax.set_xlabel('Scenario')
    ax.set_ylabel('Parameter')
    labels = [get_scenario_label(key) for key in prevented_infs.keys()]
    ax.set_xticks(np.arange(len(prevented_infs)), labels=labels)
    ax.set_yticks(
        np.arange(len(samples)),
        labels=['$R_0$', r'$\gamma$', r'$\beta$'],
        fontsize='large',
    )

    fig.savefig(png_file, dpi=300, **png_kwargs())


def plot_prevented_infection_correlations_bars(contexts, results, png_file):
    """
    Plot Spearman correlation coefficients between the numbers of prevented
    infections in each scenario, and model parameters that characterise the
    pathogen (i.e., parameters that are not related to the interventions).
    """
    prevented_infs = get_prevented_infections(results, relative_to='Baseline')
    samples = {
        'R0': contexts['Baseline'].data['prior']['R0'],
        'gamma': contexts['Baseline'].data['prior']['gamma'],
    }
    samples['beta'] = samples['R0'] * samples['gamma']

    corrs = np.zeros((len(samples), len(prevented_infs)))
    for param_ix, values in enumerate(samples.values()):
        for scenario_ix, num_prevented in enumerate(prevented_infs.values()):
            coeff = scipy.stats.spearmanr(num_prevented, values).statistic
            corrs[param_ix, scenario_ix] = coeff

    palette = mpl.colormaps['Pastel2']
    bar_width = 0.3
    fig, ax = plt.subplots(layout='constrained', figsize=[4.8, 4.8])

    for ix, scenario in enumerate(prevented_infs):
        # Plot the correlation coefficients for this scenario as bars.
        corrcoefs = corrs[:, ix]
        bars = ax.barh(
            np.arange(len(samples)) + ix * bar_width,
            width=corrcoefs,
            height=bar_width,
            color=palette(ix),
            label=get_scenario_label(scenario),
        )

        # NOTE: we position labels differently for small and large values.
        # Labels for large values are placed within the bars, while labels for
        # small values are placed adjacent to the bars.
        large_corrcoefs = [
            f'{corrcoef:0.3f}' if abs(corrcoef) > 0.2 else ''
            for corrcoef in corrcoefs
        ]
        ax.bar_label(
            bars,
            labels=large_corrcoefs,
            padding=-40,
        )
        small_corrcoefs = [
            f'{corrcoef:0.3f}' if abs(corrcoef) < 0.2 else ''
            for corrcoef in corrcoefs
        ]
        ax.bar_label(
            bars,
            labels=small_corrcoefs,
            padding=8,
        )

    # Ensure that the x-axis spans [-1, 1] by plotting invisible points (this
    # adds some padding to the axis limits).
    ax.scatter(x=[-1, 1], y=[0, 0], alpha=0)
    ax.set_xticks([-1, -0.5, 0, 0.5, 1.0])

    # Display parameter names on the left-hand side of the plot.
    ax.set_yticks(
        np.arange(len(samples)) + 0.5 * bar_width,
        labels=['$R_0$', r'$\gamma$', r'$\beta$'],
        fontsize='large',
    )

    axis_linewidth = mpl.rcParams['axes.linewidth']
    ax.axvline(x=0, color='black', linewidth=axis_linewidth)
    ax.legend(loc='best')
    ax.set_xlabel('Rank correlation with infections prevented')

    fig.savefig(png_file, dpi=300, **png_kwargs())


def png_kwargs():
    """
    Return a dictionary of keyword arguments for ``Figure.savefig`` that avoid
    storing metadata in PNG images, to ensure that they are reproducible.
    """
    return {'pil_kwargs': {'exif': None, 'pnginfo': None}}
