import numpy as np
import pytest

from pypfilt.state import repack


def test_repack_flat_float():
    xs = np.array(
        [(1.2, 2.2, 3.2), (4.2, 5.2, 6.2)],
        dtype=[('x', float), ('y', float), ('z', float)],
    )
    ys = repack(xs)
    assert ys.shape == (2, 3)
    assert ys.dtype == np.dtype(float)
    assert np.array_equal(ys, np.array([[1.2, 2.2, 3.2], [4.2, 5.2, 6.2]]))


def test_repack_flat_int():
    xs = np.array(
        [(1, 2, 3), (4, 5, 6)], dtype=[('x', int), ('y', int), ('z', int)]
    )
    ys = repack(xs, astype=int)
    assert ys.shape == (2, 3)
    assert ys.dtype == np.dtype(int)
    assert np.array_equal(ys, np.array([[1, 2, 3], [4, 5, 6]], dtype=int))


def test_repack_flat_int_as_float():
    xs = np.array(
        [(1, 2, 3), (4, 5, 6)], dtype=[('x', int), ('y', int), ('z', int)]
    )
    # NOTE: view integers as floats will cause trouble.
    with pytest.raises(ValueError, match='Fields .* are not compatible'):
        repack(xs, astype=float)


def test_repack_nested_float():
    xs = np.array(
        [(1.2, (2.2, 3.2)), (4.2, (5.2, 6.2))],
        dtype=[('x', float), ('y', float, 2)],
    )
    ys = repack(xs)
    assert ys.shape == (2, 3)
    assert ys.dtype == np.dtype(float)
    assert np.array_equal(ys, np.array([[1.2, 2.2, 3.2], [4.2, 5.2, 6.2]]))


def test_repack_nested_int():
    xs = np.array(
        [(1, (2, 3)), (4, (5, 6))], dtype=[('x', int), ('y', int, 2)]
    )
    ys = repack(xs, astype=int)
    assert ys.shape == (2, 3)
    assert ys.dtype == np.dtype(int)
    assert np.array_equal(ys, np.array([[1, 2, 3], [4, 5, 6]], dtype=int))


def test_repack_nested_int_as_float():
    xs = np.array(
        [(1, (2, 3)), (4, (5, 6))], dtype=[('x', int), ('y', int, 2)]
    )
    with pytest.raises(ValueError, match='Fields .* are not compatible'):
        repack(xs, astype=float)
